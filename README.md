# Мульти-тул

## 1. Функции

- regex - проверка регулярных выражений 
- UUID - генератор Генератор
- JSON - генератор документации по json-схеме
- ФП СУП - генератор документации по json-конфигу ФП СУП; поиск изменений

## 2. Разработчик

[Зайков Дмитрий](https://gitlab.com/d.a.zaykov)

## 3. Зависимости
 
- Java 8 и выше

Ubuntu 20.4 / Debian 11
```
# apt install openjdk-11-jre
```

Windows

[https://www.java.com/ru/download/manual.jsp](https://www.java.com/ru/download/manual.jsp)


## 4. Запуск

```
$ java -jar multitool-2022.03.29.jar
```

## 5. Релизы

[multitool-2022.03.09.jar](https://www.zdame.ru/res/multitool/multitool-2022.03.09.jar)

- Первая версия

[multitool-2022.03.10.jar](https://www.zdame.ru/res/multitool/multitool-2022.03.10.jar)

- JSON: 
  - Обработка атрибута "format" 
  - Скрыто отображение $ref
  - Столбец type теперь второй по счету

[multitool-2022.03.14.jar](https://www.zdame.ru/res/multitool/multitool-2022.03.14.jar)

- JSON:
    - Исправления по количеству повторений items[]
    - Обработка anyOf, oneOf

[multitool-2022.03.21.jar](https://www.zdame.ru/res/multitool/multitool-2022.03.21.jar)

- JSON:
	- Добавлена опция "Показать корень"

[multitool-2022.03.25.jar](https://www.zdame.ru/res/multitool/multitool-2022.03.25.jar)

- JSON:
	- fix: Исправлено дублирование при отображении дочерних элементов массива

[multitool-2022.03.29.jar](https://www.zdame.ru/res/multitool/multitool-2022.03.29.jar)

- JSON:
	- Добавлена обработка элементов examples, default

[multitool-2022.06.26.jar](https://www.zdame.ru/res/multitool/multitool-2022.06.26.jar)

- Новая фича - генератор документации по конфигу ФП СУП; поиск изменений  

[multitool-2022.06.29.jar](https://www.zdame.ru/res/multitool/multitool-2022.06.29.jar)

- ФП СУП:
    - Учтен множественный bundle
    - Добавлен поиск дублей

[multitool-2022.07.27.jar](https://www.zdame.ru/res/multitool/multitool-2022.07.27.jar)

- ФП СУП:
	- Добавлен разделитель "----" для значений параметров с несколькими бандлами

[multitool-2023.04.15.jar](https://www.zdame.ru/res/multitool/multitool-2023.04.15.jar)

- JSON:
	- Исправление расчета кратности, если required находится внутри $ref 

[multitool-2023.10.22.jar](https://www.zdame.ru/res/multitool/multitool-2023.10.22.jar)

- JSON:
	- Добавлен инклюд $ref из внешних файлов  

[multitool-2023.11.23.jar](https://www.zdame.ru/res/multitool/multitool-2023.11.23.jar)

- JSON:
	- Доработка oneOf  

