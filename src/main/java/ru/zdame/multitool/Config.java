package ru.zdame.multitool;

import java.util.ArrayList;

// Чтение параметров конфига
public class Config {
	private String fileName;
	private ArrayList<String> lstConf;

	public Config(String fileName) {
		this.fileName = Utils.isNull(fileName, "");

		// Чтение конфига
		this.lstConf = Utils.readFileToList(this.fileName);
	}

	// Запись конфига на диск
	public void flush() {
		if (this.lstConf == null)
			this.lstConf = new ArrayList<String>();
		Utils.writeListToFile(this.fileName, this.lstConf, false);
	}

	public int getParamIndex(String sParam) {
		sParam = Utils.isNull(sParam, "");

		if (this.lstConf == null)
			return -1;
		if (this.lstConf.size() == 0)
			return -1;

		try {
			int i = -1;
			for (String s: this.lstConf) {
				i++;
				s = Utils.isNull(s, "").trim();

				// Определить ключ и значение параметра
				Split sp = new Split(s, '=', "");
				String s1 = sp.s1.trim(); // Это искомый ключ
				String s2 = sp.s2.trim();

				if (s1.equals(sParam)) {
					// Отсечь из значения параметра комментарии #
					sp = new Split(s2, '#', "");
					return i;
				}
			}
			return -1;
		}
		catch (Exception ex) {
			System.out.println("Config.getParamIndex(" + sParam + "): Error: " + ex.getMessage());
			return -1;
		}
	}

	public String getParam(String sParam, String sDef) {
		String res = sDef;
		sParam = Utils.isNull(sParam, "");

		if (this.lstConf == null)
			return sDef;
		if (this.lstConf.size() == 0)
			return sDef;

		try {
			int k = getParamIndex(sParam);
			if (k < 0)
				return sDef;

			// Определить ключ и значение параметра
			String s = Utils.isNull(this.lstConf.get(k), "").trim();
			Split sp = new Split(s, '=', "");
			String s1 = sp.s1.trim();
			String s2 = sp.s2.trim(); // Это значение параметра с коментами
			// Отсечь из значения параметра комментарии #
			sp = new Split(s2, '#', "");
			return sp.s1.trim(); // Это искомое значение
		}
		catch (Exception ex) {
			System.out.println("Config.getParam(" + sParam + "): Error: " + ex.getMessage());
			return sDef;
		}
	}

	public void setParam(String sParam, String sValue) {
		sParam = Utils.isNull(sParam, "");
		sValue = Utils.isNull(sValue, "");

		if (this.lstConf == null)
			this.lstConf = new ArrayList<String>();

		try {
			int k = getParamIndex(sParam);
			if (k < 0)
				this.lstConf.add(sParam + "=" + sValue);
			else
				this.lstConf.set(k, sParam + "=" + sValue);
		}
		catch (Exception ex) {
			System.out.println("Config.setParam(" + sParam + "): Error: " + ex.getMessage());
		}
	}




}