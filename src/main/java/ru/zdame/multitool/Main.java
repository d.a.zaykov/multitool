package ru.zdame.multitool;

import java.io.File;
//import java.nio.file.Path;
import java.nio.file.Paths;


public class Main {

	public static final String APP_NAME = "multitool";
	public static final String APP_VERSION = "2023.11.23";
	public static String APP_FULLNAME = ""; // Полное имя файла приложения
	public static String APP_PATH = ""; // Полный путь до приложения
	public static Config config = null;

	public static void main(String[] args) {
		// Старт
		Logger.add("");
		Logger.add("");
		Logger.add("==================================================================");
		Logger.add("Старт: " + APP_NAME + "-" + APP_VERSION);

		try {
			//APP_PATH = System.getProperty("user.dir"); // текущий рабочий каталог
			//APP_PATH = System.getProperty("user.home"); // домашний каталог пользователя
			// Полное имя файла приложения
			APP_FULLNAME = Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
			// Полный путь до приложения
			APP_PATH = new File(APP_FULLNAME).getParent();
		}
		catch (Exception ex) {}
		Logger.add("APP_FULLNAME=" + APP_FULLNAME);
		Logger.add("APP_PATH=" + APP_PATH);

		// Конфиг
		String confName = Paths.get(APP_PATH, "multitool.conf").toString();
		Logger.add("confName=" + confName);
		config = new Config(confName);

		// Запуск графики
		new MainForm();
	}

}


