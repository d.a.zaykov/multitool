package ru.zdame.multitool;

import com.fasterxml.jackson.databind.JsonNode;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.text.StringEscapeUtils;


public class Utils {

	// Подготовка строки. Замена null на sDef
	public static String isNull(String s, String sDef) {
		if (s == null)
			return sDef;
		else
			return s;
	}

	// Чтение строки из JsonNode
	public static String jsonReadStr(JsonNode node, String param, String sDef) {
		try {
			if (node == null)
				return sDef;
			return  isNull(node.get(param).asText(), sDef);
		}
		catch (Exception ex) {
			return sDef;
		}
	}

	// Подготовка строки. Обрезание до max символов
	public static String strtrunc(String s, int max) {
		String s2 = isNull(s, "");
		int n = s2.length();
		if (n > max)
			return s2.substring(0, max);
		else
			return s2;
	}

	public static String date2str(LocalDateTime dt) {
		DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		return dt.format(df);
	}

	// Прочитать текстовый файл
	public static String readFileToStr(String fileName) {
		try {
			// for sdk 11
			//return Files.readString(Paths.get(fileName), StandardCharsets.UTF_8);
			// for sdk 1.8
			ArrayList<String> lst = readFileToList(fileName);
			String s = "";
			if (lst != null)
				for (String s2 : lst) {
					s += s2 + "\n";
				}
			return s;
		}
		catch (Exception ex) {
			return "";
		}
	}

	// Прочитать текстовый файл
	public static ArrayList<String> readFileToList(String fileName) {
		try {
			List<String> lst = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
			ArrayList<String> res = new ArrayList<String>(lst);
			return res;
		}
		catch (Exception ex) {
			return null;
		}
	}

	// Записать список в текстовый файл
	public static boolean writeListToFile(String fileName, ArrayList<String> lst, boolean append) {
		try {
			if (append)
				Files.write(Paths.get(fileName), lst, StandardCharsets.UTF_8, StandardOpenOption.APPEND);
			else
				Files.write(Paths.get(fileName), lst, StandardCharsets.UTF_8, StandardOpenOption.WRITE);

			/*FileWriter f = new FileWriter(fileName, append);
			try {
				if (lst != null) {
					for (String s : lst)
						f.write(s);
					f.write("\n");
				}
			} finally {
				f.flush();
				f.close();
			}*/
			return true;
		}
		catch (Exception ex) {
			System.out.println("Utils.writeListToFile: Error: " + ex.getMessage());
			return false;
		}
	}

	// Экранирование символов HTML
	public static String htmlEscape(String s) {
		String res = StringEscapeUtils.escapeHtml4(s);
		res = res.replaceAll("\n", "<br>");
		return res;
	}

	// Найти существующий путь
	public static String findParentPath(String sPath) {
		sPath = isNull(sPath, "").trim();
		if (sPath.isEmpty())
			return "";

		File file = new File(sPath);
		if (file.isDirectory() && file.exists())
			return sPath;
		else
			return findParentPath(Paths.get(sPath).getParent().toString());
	}

}


