package ru.zdame.multitool;

import java.io.File;
import java.util.*;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.TitledBorder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


public class MainForm extends JFrame {

	private static JFrame frame = null;
	private static JTextField txtRegexInput = null;
	private static JTextArea txtRegexText = null;
	private static JTextArea txtRegexRes = null;
	private static JPanel panelRegexMatch = null;
	private static JTextArea txtUUIDRes = null;
	private static JSpinner spinnerUUIDCount = null;
	private static JPanel panelJSONRes = null;
	private static JEditorPane txtJSONRes = null;
	private static JScrollPane scrollJSONRes = null;
	private static File fileJSON = null;
	private static File fileSUP_old = null;
	private static File fileSUP_new = null;
	private static JCheckBox cbJSONRoot = null;
	private static int corrlevel = 0;
	private static JCheckBox cbSUPSort = null;
	private static JPanel panelSUPRes = null;
	private static JEditorPane txtSUPRes = null;
	private static JScrollPane scrollSUPRes = null;


	// Класс для одного объекта JSON-документации
	public static class DocNode {
		JsonNode jsonNode = null;
		JsonNode requiredNode = null;
		public int level;
		public String path;
		public String id;
		public String description;
		public String type;
		public String arrayof;
		public String minLength;
		public String maxLength;
		public String minimum;
		public String maximum;
		public String minItems;
		public String maxItems;
		public String minProperties;
		public String maxProperties;
		public String uniqueItems;
		public String pattern;
		public String format;
		public String senum;
		public String example;
		public String examples;
		public String example_default;
		public int minCount;
		public int maxCount;
		public String ref;
		public ObjectMapper mapperRef;
		public ArrayList<DocNode> childs;

		DocNode() {
			level = 1;
			path = "";
			id = "";
			description = "";
			type = "";
			arrayof = "";
			minLength = "";
			maxLength = "";
			minimum = "";
			maximum = "";
			minItems = "";
			maxItems = "";
			minProperties = "";
			maxProperties = "";
			uniqueItems = "";
			pattern = "";
			format = "";
			senum = "";
			example = "";
			examples = "";
			example_default = "";
			minCount = 0;
			maxCount = 1;
			ref = "";
			ObjectMapper mapperRef = null;
			childs = new ArrayList<DocNode>();
		}
	}

	private static DocNode rootDoc = null; // Корневой элемент json
	private static ObjectMapper mapper = null;
	private static String sDoc = null; // Сформированный html
	private static int maxDocLevel = 1; // Максимальная глубина дерева DocNode


	// Класс для одного параметра СУП
	public static class SUP {
		public String name;
		public String description;
		public String type;
		public Boolean isList;
		public String roles;
		public String channel;
		public String values;

		SUP() {
			name = "";
			description = "";
			type = "";
			isList = false;
			roles = "";
			channel = "";
			values = "";
		}
	}

	public static ArrayList<SUP> SUP_old = null; // Старый конфиг
	public static ArrayList<SUP> SUP_new = null; // Новый конфиг



	public MainForm() {
		JFrame.setDefaultLookAndFeelDecorated(true); // Декоративное окно
		frame = new JFrame(Main.APP_NAME + "-" + Main.APP_VERSION);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Закрыть окно при нажатии по крестику
		//frame.setLocationRelativeTo(null); // Окно расположит по центру экрана
		frame.setSize(1000, 700);
		frame.setVisible(true);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.WRAP_TAB_LAYOUT);
		frame.add(tabbedPane);


		// Вкладка regex
		JPanel panelRegex = new JPanel();
		tabbedPane.add("regex", panelRegex);
		panelRegex.setLayout(new BorderLayout());

		// Поле ввода
		JPanel panelRegexInput = new JPanel();
		panelRegex.add(panelRegexInput, BorderLayout.NORTH);
		panelRegexInput.setLayout(new BorderLayout());
		panelRegexInput.setBorder(new TitledBorder("Регулярное выражение"));

		txtRegexInput = new JTextField("^.*$");
		panelRegexInput.add(txtRegexInput, BorderLayout.NORTH);
		txtRegexInput.setFont(new Font("Consolas", Font.PLAIN, 20));

		txtRegexInput.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent e) {}
			public void keyReleased(KeyEvent e) {
				REGEX_Check();
			}
			public void keyTyped(KeyEvent e) {}
		});

		// Проверяемый текст, Совпадения
		JPanel panelRegex2 = new JPanel();
		panelRegex.add(panelRegex2, BorderLayout.CENTER);
		panelRegex2.setLayout(new GridLayout(2, 1, 5, 12));

		// Проверяемый текст
		JPanel panelRegexText = new JPanel();
		panelRegex2.add(panelRegexText);
		panelRegexText.setBorder(new TitledBorder("Проверяемый текст"));
		panelRegexText.setLayout(new BorderLayout());

		txtRegexText = new JTextArea("текст");
		txtRegexText.setFont(new Font("Consolas", Font.PLAIN, 20));

		txtRegexText.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent e) {}
			public void keyReleased(KeyEvent e) {
				REGEX_Check();
			}
			public void keyTyped(KeyEvent e) {}
		});

		JScrollPane scrollRegexText = new JScrollPane(txtRegexText);
		panelRegexText.add(scrollRegexText, BorderLayout.CENTER);

		// Совпадения
		panelRegexMatch = new JPanel();
		panelRegex2.add(panelRegexMatch);
		panelRegexMatch.setBorder(new TitledBorder("Совпадения"));
		panelRegexMatch.setLayout(new BorderLayout());

		txtRegexRes = new JTextArea("");
		txtRegexRes.setEditable(false);
		txtRegexRes.setBackground(Color.LIGHT_GRAY);
		txtRegexRes.setFont(new Font("Consolas", Font.PLAIN, 20));

		JScrollPane scrollRegexRes = new JScrollPane(txtRegexRes);
		panelRegexMatch.add(scrollRegexRes, BorderLayout.CENTER);

		// Первый старт проверки регулярки
		REGEX_Check();




		// Вкладка UUID
		JPanel panelUUID = new JPanel();
		tabbedPane.add("UUID", panelUUID);
		panelUUID.setLayout(new BorderLayout());

		// Опции
		JPanel panelUUIDOpt = new JPanel();
		panelUUID.add(panelUUIDOpt, BorderLayout.NORTH);
		panelUUIDOpt.setLayout(new FlowLayout(FlowLayout.LEFT));
		panelUUIDOpt.setBorder(new TitledBorder("Опции"));

		// Количество
		JLabel labelUUIDCount = new JLabel("Количество");
		panelUUIDOpt.add(labelUUIDCount);

		spinnerUUIDCount = new JSpinner(new SpinnerNumberModel(10, 1, 100000, 1));
		panelUUIDOpt.add(spinnerUUIDCount);

		JButton btnUUID = new JButton("Сгенерировать");
		panelUUIDOpt.add(btnUUID);
		btnUUID.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UUID_Generate();
			}
		});

		// Результат
		JPanel panelUUIDRes = new JPanel();
		panelUUID.add(panelUUIDRes, BorderLayout.CENTER);
		panelUUIDRes.setLayout(new BorderLayout());
		panelUUIDRes.setBorder(new TitledBorder("Результат"));

		txtUUIDRes = new JTextArea("");
		txtUUIDRes.setEditable(false);
		txtUUIDRes.setBackground(Color.LIGHT_GRAY);
		txtUUIDRes.setFont(new Font("Consolas", Font.PLAIN, 20));

		JScrollPane scrollUUIDRes = new JScrollPane(txtUUIDRes);
		panelUUIDRes.add(scrollUUIDRes, BorderLayout.CENTER);



		// Вкладка JSON
		JPanel panelJSON = new JPanel();
		tabbedPane.add("JSON", panelJSON);
		panelJSON.setLayout(new BorderLayout());

		// json-схема
		JPanel panelJSONOpt = new JPanel();
		panelJSON.add(panelJSONOpt, BorderLayout.NORTH);
		panelJSONOpt.setLayout(new FlowLayout(FlowLayout.LEFT));
		panelJSONOpt.setBorder(new TitledBorder("json-схема"));

		// Показать id
		cbJSONRoot = new JCheckBox("Показать корень", true);
		panelJSONOpt.add(cbJSONRoot);

		// Выбрать файл
		JButton btnJSONOF = new JButton("Выбрать...");
		panelJSONOpt.add(btnJSONOF);
		btnJSONOF.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Взять из конфига последний путь
				String json_dir = Main.config.getParam("json_dir", "");
				Logger.add("get json_dir=" + json_dir);
				// Найти существующий путь
				json_dir = Utils.findParentPath(json_dir);
				Logger.add("open json_dir=" + json_dir);
				// Диалог
				JFileChooser fc = new JFileChooser(json_dir);
				fc.setDialogTitle("Выбор файла");
				fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
				int res = fc.showOpenDialog(frame);
				if (res == JFileChooser.APPROVE_OPTION) {
					json_dir = fc.getCurrentDirectory().toString();
					Logger.add("set json_dir=" + json_dir);
					Main.config.setParam("json_dir", json_dir);
					Main.config.flush();
					fileJSON = fc.getSelectedFile();
					//panelJSONOpt.setBorder(new TitledBorder("json-схема: " + fileJSON.toString()));
					txtJSONRes.setText(fileJSON.toString());
				}
				else
				{
					fileJSON = null;
					//panelJSONOpt.setBorder(new TitledBorder("json-схема"));
					txtJSONRes.setText("");
				}
			}
		});

		// Сгенерировать документацию
		JButton btnJSON = new JButton("Сгенерировать документацию");
		panelJSONOpt.add(btnJSON);
		btnJSON.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JSON_Read();
			}
		});


		// Результат
		panelJSONRes = new JPanel();
		panelJSON.add(panelJSONRes, BorderLayout.CENTER);
		panelJSONRes.setLayout(new BorderLayout());
		panelJSONRes.setBorder(new TitledBorder("Результат"));

		txtJSONRes = new JEditorPane();
		txtJSONRes.setContentType("text/html");
		txtJSONRes.setEditable(false);
		txtJSONRes.setBackground(Color.LIGHT_GRAY);
		txtJSONRes.setFont(new Font("Consolas", Font.PLAIN, 20));

		scrollJSONRes = new JScrollPane(txtJSONRes);
		panelJSONRes.add(scrollJSONRes, BorderLayout.CENTER);




		// Вкладка ФП СУП
		JPanel panelSUP = new JPanel();
		tabbedPane.add("ФП СУП", panelSUP);
		panelSUP.setLayout(new BorderLayout());

		// json-конфиг
		JPanel panelSUPOpt = new JPanel();
		panelSUP.add(panelSUPOpt, BorderLayout.NORTH);
		panelSUPOpt.setLayout(new FlowLayout(FlowLayout.LEFT));
		panelSUPOpt.setBorder(new TitledBorder("json-конфиг"));

		// Сортировать
		cbSUPSort = new JCheckBox("Сортировать", false);
		panelSUPOpt.add(cbSUPSort);

		// Выбрать старый
		JButton btnSUP_old = new JButton("Старый...");
		panelSUPOpt.add(btnSUP_old);
		btnSUP_old.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Взять из конфига последний путь
				String sup_old_dir = Main.config.getParam("sup_old_dir", "");
				Logger.add("get sup_old_dir=" + sup_old_dir);
				// Найти существующий путь
				sup_old_dir = Utils.findParentPath(sup_old_dir);
				Logger.add("open sup_old_dir=" + sup_old_dir);
				// Диалог
				JFileChooser fc = new JFileChooser(sup_old_dir);
				fc.setDialogTitle("Выбор файла");
				fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
				int res = fc.showOpenDialog(frame);
				if (res == JFileChooser.APPROVE_OPTION) {
					sup_old_dir = fc.getCurrentDirectory().toString();
					Logger.add("set sup_old_dir=" + sup_old_dir);
					Main.config.setParam("sup_old_dir", sup_old_dir);
					Main.config.flush();
					fileSUP_old = fc.getSelectedFile();
					//panelSUPOpt.setBorder(new TitledBorder("json-конфиг: " + SUP_PathToStr(fileSUP_old, fileSUP_new)));
					txtSUPRes.setText(SUP_PathToStr(fileSUP_old, fileSUP_new));
				}
				else
				{
					fileSUP_old = null;
					//panelSUPOpt.setBorder(new TitledBorder("json-конфиг: " + SUP_PathToStr(fileSUP_old, fileSUP_new)));
					txtSUPRes.setText(SUP_PathToStr(fileSUP_old, fileSUP_new));
				}
			}
		});

		// Выбрать новый
		JButton btnSUP_new = new JButton("Новый...");
		panelSUPOpt.add(btnSUP_new);
		btnSUP_new.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Взять из конфига последний путь
				String sup_old_new = Main.config.getParam("sup_old_new", "");
				Logger.add("get sup_old_new=" + sup_old_new);
				// Найти существующий путь
				sup_old_new = Utils.findParentPath(sup_old_new);
				Logger.add("open sup_old_new=" + sup_old_new);
				// Диалог
				JFileChooser fc = new JFileChooser(sup_old_new);
				fc.setDialogTitle("Выбор файла");
				fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
				int res = fc.showOpenDialog(frame);
				if (res == JFileChooser.APPROVE_OPTION) {
					sup_old_new = fc.getCurrentDirectory().toString();
					Logger.add("set sup_old_new=" + sup_old_new);
					Main.config.setParam("sup_old_new", sup_old_new);
					Main.config.flush();
					fileSUP_new = fc.getSelectedFile();
					//panelSUPOpt.setBorder(new TitledBorder("json-конфиг: " + SUP_PathToStr(fileSUP_old, fileSUP_new)));
					txtSUPRes.setText(SUP_PathToStr(fileSUP_old, fileSUP_new));
				}
				else
				{
					fileSUP_new = null;
					//panelSUPOpt.setBorder(new TitledBorder("json-конфиг: " + SUP_PathToStr(fileSUP_old, fileSUP_new)));
					txtSUPRes.setText(SUP_PathToStr(fileSUP_old, fileSUP_new));
				}
			}
		});

		// Сгенерировать документацию
		JButton btnSUP = new JButton("Сгенерировать документацию");
		panelSUPOpt.add(btnSUP);
		btnSUP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SUP_old = new ArrayList<SUP>();
				SUP_new = new ArrayList<SUP>();
				SUP_Read(fileSUP_old, SUP_old);
				SUP_Read(fileSUP_new, SUP_new);
				SUP_GenerateHTML();
			}
		});


		// Результат
		panelSUPRes = new JPanel();
		panelSUP.add(panelSUPRes, BorderLayout.CENTER);
		panelSUPRes.setLayout(new BorderLayout());
		panelSUPRes.setBorder(new TitledBorder("Результат"));

		txtSUPRes = new JEditorPane();
		txtSUPRes.setContentType("text/html");
		txtSUPRes.setEditable(false);
		txtSUPRes.setBackground(Color.LIGHT_GRAY);
		txtSUPRes.setFont(new Font("Consolas", Font.PLAIN, 20));

		scrollSUPRes = new JScrollPane(txtSUPRes);
		panelSUPRes.add(scrollSUPRes, BorderLayout.CENTER);



	}


	// Проверка регулярного выражения
	// Регулярка - txtRegexInput
	// Проверяемый текст - txtRegexText
	// Результат - txtRegexRes
	private static void REGEX_Check() {
		//txtRegexRes.setText(txtRegexInput.getText() + "\n" + txtRegexText.getText());

		// Проверка полного совпадения
		boolean isMatch = false;
		try {
			isMatch = Pattern.matches(txtRegexInput.getText(), txtRegexText.getText());
		}
		catch (Exception ex) {
			txtRegexRes.setText(ex.getMessage());
		}
		if (isMatch)
			panelRegexMatch.setBackground(Color.GREEN);
		else
			panelRegexMatch.setBackground(Color.RED);

		// Найти все совпадения
		txtRegexRes.setText("");
		try {
			Pattern pattern = Pattern.compile(txtRegexInput.getText());
			Matcher matcher = pattern.matcher(txtRegexText.getText());
			boolean isFirst = true;
			while (matcher.find()) {
				if (isFirst) {
					isFirst = false;
					txtRegexRes.append(txtRegexText.getText().substring(matcher.start(), matcher.end()));
				}
				else {
					txtRegexRes.append("\n" + txtRegexText.getText().substring(matcher.start(), matcher.end()));
				}
			}
		}
		catch (Exception ex) {
			txtRegexRes.setText(ex.getMessage());
		}
	}


	// Генератор UUID
	private static void UUID_Generate() {
		txtUUIDRes.setText("");
		int n = (int)spinnerUUIDCount.getValue();
		for (int i = 0; i < n; i++) {
			if (i == 0)
				txtUUIDRes.append("" + UUID.randomUUID());
			else
				txtUUIDRes.append("\n" + UUID.randomUUID());
		}
	}


	// Чтение json-схемы
	private static void JSON_Read() {
		txtJSONRes.setText("");

		String sfile = "";
		if (fileJSON != null)
			sfile = fileJSON.toString();
		if (sfile.isEmpty()) {
			return;
		}

		try {
			// Прочитать json
			String sJson = Utils.readFileToStr(sfile);
			//txtJSONRes.setText(sJson);
			//txtJSONRes.setText(fileJSON.getName());

			// Получить корневой объект
			mapper = new ObjectMapper();
			JsonNode rootNode = mapper.readValue(sJson, JsonNode.class);
			JsonNode definitionsNode = null;
			if (rootNode != null) {
				rootDoc = new DocNode();
				rootDoc.jsonNode = rootNode;
				rootDoc.requiredNode = rootNode.get("required");
				definitionsNode = rootNode.get("definitions");
				rootDoc.level = 1;
				rootDoc.id = fileJSON.getName();
				String s = Utils.jsonReadStr(rootNode, "id" ,"").trim();
				if (s.isEmpty())
				{
					String s2 = Utils.jsonReadStr(rootNode, "ident" ,"").trim();
					if (!s2.isEmpty())
						rootDoc.id = s2;
				}
				else
					rootDoc.id = s;
				rootDoc.minCount = 0;
				JSON_ReadByNode(rootDoc, definitionsNode);

				// Сгенерить документацию
				JSON_GenerateHTML(rootDoc);
			}
		}
		catch (Exception ex) {
			txtJSONRes.setText(ex.getMessage());
		}
	}

	// Рекурсивное чтение объекта json-схемы
	private static void JSON_ReadByNode(DocNode docNode, JsonNode definitionsNode) {
		if (docNode == null)
			return;
		if (docNode.jsonNode == null)
			return;

		try {
			// Собственный description
			docNode.description = Utils.jsonReadStr(docNode.jsonNode, "description" ,"").trim();

			// Сначала проверить не ссылается ли объект на другой тип
			docNode.ref = Utils.jsonReadStr(docNode.jsonNode, "$ref" ,"").trim();
			if (!docNode.ref.isEmpty()) {
				// Найти ссылку на новую ноду по значению типа "#/definitions/FlatArrayType"
				Boolean isFind = false;

				// Убрать символ #
				Split sp1 = new Split(docNode.ref, '#', "");
				String s1 = sp1.s1.trim(); // ""
				String s2 = sp1.s2.trim(); // "/definitions/FlatArrayType"

				// Убрать символ /
				sp1 = new Split(s2, '/', "");
				s1 = sp1.s1.trim(); // ""
				s2 = sp1.s2.trim(); // "definitions/FlatArrayType"

				// Убрать "definitions/"
				sp1 = new Split(s2, '/', "");
				s1 = sp1.s1.trim(); // "definitions"
				s2 = sp1.s2.trim(); // "FlatArrayType"
				if (s1.equals("definitions")) {
					// Переопределить текущую ноду
					if (definitionsNode != null)
						docNode.jsonNode = definitionsNode.get(s2);
					if (docNode.jsonNode == null)
						return;

					// Переопределить ноду required
					isFind = true;
					docNode.requiredNode = docNode.jsonNode.get("required");
				}
				// Если не найден ref в текущем файле, то поискать во внешнем файле
				if (!isFind) {
					// Попытаться прочитать файл ref
					String sfile = "";
					if (fileJSON != null) {
						sfile = fileJSON.getParent();
						File f = new File(sfile, docNode.ref);
						sfile = f.toString();
					}
					String sJson = Utils.readFileToStr(sfile);

					// Переопределить ноду
					if (!sJson.isEmpty()) {
						//docNode.ref += " read";

						docNode.mapperRef = new ObjectMapper();
						JsonNode rootNode = docNode.mapperRef.readValue(sJson, JsonNode.class);
						if (rootNode != null) {
							docNode.jsonNode = rootNode;
							docNode.requiredNode = rootNode.get("required");
							definitionsNode = rootNode.get("definitions");
						}
					}
				}
			}

			// description из типа ref
			if (docNode.description.isEmpty())
				docNode.description = Utils.jsonReadStr(docNode.jsonNode, "description" ,"").trim();

			docNode.type = Utils.jsonReadStr(docNode.jsonNode, "type" ,"").trim();
			docNode.minLength = Utils.jsonReadStr(docNode.jsonNode, "minLength" ,"").trim();
			docNode.maxLength = Utils.jsonReadStr(docNode.jsonNode, "maxLength" ,"").trim();
			docNode.minimum = Utils.jsonReadStr(docNode.jsonNode, "minimum" ,"").trim();
			docNode.maximum = Utils.jsonReadStr(docNode.jsonNode, "maximum" ,"").trim();
			if (docNode.minItems.isEmpty())
				docNode.minItems = Utils.jsonReadStr(docNode.jsonNode, "minItems" ,"").trim();
			if (docNode.maxItems.isEmpty())
				docNode.maxItems = Utils.jsonReadStr(docNode.jsonNode, "maxItems" ,"").trim();
			docNode.uniqueItems = Utils.jsonReadStr(docNode.jsonNode, "uniqueItems" ,"").trim();
			docNode.minProperties = Utils.jsonReadStr(docNode.jsonNode, "minProperties" ,"").trim();
			docNode.maxProperties = Utils.jsonReadStr(docNode.jsonNode, "maxProperties" ,"").trim();
			docNode.pattern = Utils.jsonReadStr(docNode.jsonNode, "pattern" ,"").trim();
			docNode.format = Utils.jsonReadStr(docNode.jsonNode, "format" ,"").trim();
			docNode.example = Utils.jsonReadStr(docNode.jsonNode, "example" ,"").trim();
			docNode.example_default = Utils.jsonReadStr(docNode.jsonNode, "default" ,"").trim();

			// examples
			docNode.examples = "";
			JsonNode examplesNode = docNode.jsonNode.get("examples");
			if (examplesNode != null) {
				if (examplesNode.isArray()) {
					for (JsonNode node2 : examplesNode) {
						String s = Utils.isNull(node2.asText(), "");
						if (docNode.examples.isEmpty())
							docNode.examples += "\"" + s + "\"";
						else
							docNode.examples += ", " + "\"" + s + "\"";
					}
				}
			}

			// enum
			docNode.senum = "";
			JsonNode enumNode = docNode.jsonNode.get("enum");
			if (enumNode != null) {
				if (enumNode.isArray()) {
					for (JsonNode node2 : enumNode) {
						String s = Utils.isNull(node2.asText(), "");
						if (docNode.senum.isEmpty())
							docNode.senum += "\"" + s + "\"";
						else
							docNode.senum += ", " + "\"" + s + "\"";
					}
				}
			}

			// Дочерние объекты items
			JsonNode itemsNode = docNode.jsonNode.get("items");
			if (docNode.type.equals("array")) {
				int minItems = 0;
				try {
					minItems = Integer.parseInt(docNode.minItems);
				}
				catch (Exception ex) {
					minItems = 0;
				}
				//if (minItems > docNode.minCount)
					//docNode.minCount = minItems;

				int maxItems = -1;
				try {
					maxItems = Integer.parseInt(docNode.maxItems);
				}
				catch (Exception ex) {
					maxItems = -1;
				}
				//docNode.maxCount = maxItems;

				// array of
				if (itemsNode != null) {
					//	docNode.arrayof = Utils.jsonReadStr(itemsNode, "type" ,"").trim();
					DocNode childDoc = new DocNode();
					childDoc.jsonNode = itemsNode;
					childDoc.requiredNode = itemsNode.get("required");
					childDoc.level = docNode.level + 1;
					if (childDoc.level > maxDocLevel)
						maxDocLevel = childDoc.level;
					childDoc.id = docNode.id + "[]";

					if (minItems > childDoc.minCount)
						childDoc.minCount = minItems;
					childDoc.maxCount = maxItems;

					docNode.childs.add(childDoc);
					JSON_ReadByNode(childDoc, definitionsNode);
				}
			}

			// Дочерние объекты properties
			JsonNode propertiesNode = null;
			//if (itemsNode != null)
			//	propertiesNode = itemsNode.get("properties");
			//else
			propertiesNode = docNode.jsonNode.get("properties");
			if (propertiesNode != null) {
				Iterator<Map.Entry<String, JsonNode>> nodes = propertiesNode.fields();
				while (nodes.hasNext()) {
					Map.Entry<String, JsonNode> entry = (Map.Entry<String, JsonNode>) nodes.next();
					String key = Utils.isNull(entry.getKey(), "").trim();
					JsonNode node = entry.getValue();

					// Проверить обязательность объекта key
					int minCount = 0;
					if (docNode.requiredNode != null) {
						if (docNode.requiredNode.isArray()) {
							for (JsonNode node2 : docNode.requiredNode) {
								String key2 = Utils.isNull(node2.asText(), "").trim();
								if (key.equals(key2))
								{
									minCount = 1;
									break;
								}
							}
						}
					}

					DocNode childDoc = new DocNode();
					childDoc.jsonNode = node;
					childDoc.requiredNode = node.get("required");
					childDoc.level = docNode.level + 1;
					if (childDoc.level > maxDocLevel)
						maxDocLevel = childDoc.level;
					childDoc.id = key;
					childDoc.minCount = minCount;
					docNode.childs.add(childDoc);
					JSON_ReadByNode(childDoc, definitionsNode);
				} // while
			} // if

			// Дочерние объекты patternProperties
			JsonNode patternPropertiesNode = docNode.jsonNode.get("patternProperties");
			if (patternPropertiesNode != null) {
				int minProperties = 0;
				try {
					minProperties = Integer.parseInt(docNode.minProperties);
				}
				catch (Exception ex) {
					minProperties = 0;
				}

				int maxProperties = -1;
				try {
					maxProperties = Integer.parseInt(docNode.maxProperties);
				}
				catch (Exception ex) {
					maxProperties = -1;
				}

				Iterator<Map.Entry<String, JsonNode>> nodes = patternPropertiesNode.fields();
				while (nodes.hasNext()) {
					Map.Entry<String, JsonNode> entry = (Map.Entry<String, JsonNode>) nodes.next();
					String key = Utils.isNull(entry.getKey(), "").trim();
					JsonNode node = entry.getValue();

					DocNode childDoc = new DocNode();
					childDoc.jsonNode = node;
					childDoc.requiredNode = node.get("required");
					childDoc.level = docNode.level + 1;
					if (childDoc.level > maxDocLevel)
						maxDocLevel = childDoc.level;
					childDoc.id = key;
					childDoc.minCount = minProperties;
					childDoc.maxCount = maxProperties;
					docNode.childs.add(childDoc);
					JSON_ReadByNode(childDoc, definitionsNode);
				} // while
			} // if


			// oneOf
			JsonNode oneOfNode = docNode.jsonNode.get("oneOf");
			if (oneOfNode != null) {
				if (oneOfNode.isArray()) {
					DocNode oneOfDoc = new DocNode();
					oneOfDoc.jsonNode = oneOfNode;
					oneOfDoc.level = docNode.level + 1;
					if (oneOfDoc.level > maxDocLevel)
						maxDocLevel = oneOfDoc.level;
					oneOfDoc.id = "Один из нижеследующих (oneOf):";
					oneOfDoc.minCount = 1;
					oneOfDoc.maxCount = 1;
					docNode.childs.add(oneOfDoc);

					for (JsonNode oneOfNode2 : oneOfNode) {
						DocNode oneOf2Doc = new DocNode();
						oneOf2Doc.jsonNode = oneOfNode2;
						oneOf2Doc.requiredNode = oneOfNode2.get("required");
						oneOf2Doc.level = oneOfDoc.level + 1;
						if (oneOf2Doc.level > maxDocLevel)
							maxDocLevel = oneOf2Doc.level;
						oneOf2Doc.id = docNode.id;
						oneOfDoc.childs.add(oneOf2Doc);

						JSON_ReadByNode(oneOf2Doc, definitionsNode);

						/*// required
						JsonNode requiredNode = oneOfNode2.get("required");
						if (requiredNode != null) {
							if (requiredNode.isArray()) {
								for (JsonNode node2 : requiredNode) {
									String key = Utils.isNull(node2.asText(), "").trim();

									DocNode oneOf3Doc = new DocNode();
									oneOf3Doc.jsonNode = null;
									oneOf3Doc.level = oneOf2Doc.level + 1;
									if (oneOf3Doc.level > maxDocLevel)
										maxDocLevel = oneOf3Doc.level;
									oneOf3Doc.id = key;
									oneOf3Doc.minCount = 1;
									oneOf2Doc.childs.add(oneOf3Doc);

									JSON_ReadByNode(oneOf3Doc, definitionsNode);
								}
							}
						}*/
					}
				}
			} // if


			// anyOf
			JsonNode anyOfNode = docNode.jsonNode.get("anyOf");
			if (anyOfNode != null) {
				if (anyOfNode.isArray()) {
					DocNode anyOfDoc = new DocNode();
					anyOfDoc.jsonNode = oneOfNode;
					anyOfDoc.level = docNode.level + 1;
					if (anyOfDoc.level > maxDocLevel)
						maxDocLevel = anyOfDoc.level;
					anyOfDoc.id = "Любой из нижеследующих (anyOf):";
					anyOfDoc.minCount = 1;
					anyOfDoc.maxCount = 1;
					docNode.childs.add(anyOfDoc);

					for (JsonNode anyOfNode2 : anyOfNode) {
						DocNode anyOf2Doc = new DocNode();
						anyOf2Doc.jsonNode = anyOfNode2;
						anyOf2Doc.level = anyOfDoc.level + 1;
						if (anyOf2Doc.level > maxDocLevel)
							maxDocLevel = anyOf2Doc.level;
						anyOf2Doc.id = docNode.id;
						anyOfDoc.childs.add(anyOf2Doc);

						JSON_ReadByNode(anyOf2Doc, definitionsNode);

						// required
						JsonNode requiredNode = anyOfNode2.get("required");
						if (requiredNode != null) {
							if (requiredNode.isArray()) {
								for (JsonNode node2 : requiredNode) {
									String key = Utils.isNull(node2.asText(), "").trim();

									DocNode anyOf3Doc = new DocNode();
									anyOf3Doc.jsonNode = null;
									anyOf3Doc.level = anyOf2Doc.level + 1;
									if (anyOf3Doc.level > maxDocLevel)
										maxDocLevel = anyOf3Doc.level;
									anyOf3Doc.id = key;
									anyOf3Doc.minCount = 1;
									anyOf2Doc.childs.add(anyOf3Doc);

									JSON_ReadByNode(anyOf3Doc, definitionsNode);
								}
							}
						}
					}
				}
			} // if

		}
		catch (Exception ex) {
			txtJSONRes.setText(ex.getMessage());
		}
	}


	// Генератор документации в формате HTML
	private static void JSON_GenerateHTML(DocNode rootDoc) {
		txtJSONRes.setText("");
		if (rootDoc == null)
			return;

		// Показать id
		if (cbJSONRoot.isSelected()) {
			corrlevel = 0;
		}
		else {
			corrlevel = -1; // Скорректировать уровень вложенности для отображения
		}

		sDoc = "<html><head><style type='text/css'>body {font-family: Sans; font-size: 14;}</style></head>";
		sDoc += "<body><table align='left' border='1' class='wrapped'>";

		// Заголовок таблицы
		sDoc += "<tr>" +
			"<th align='left' valign='top' colspan='" + (maxDocLevel + corrlevel) + "'>Элемент</th>" +
			"<th align='left' valign='top'>Тип</th>" +
			"<th align='left' valign='top'>Описание</th>" +
			"<th align='left' valign='top'>Кратность</th>" +
			"</tr>";


		// Показать id
		if (cbJSONRoot.isSelected()) {
			JSON_GenerateHTMLByNode(rootDoc);
		}
		else {
			// Пройти по дочерним объектам
			for (DocNode node : rootDoc.childs) {
				JSON_GenerateHTMLByNode(node);
			}
		}

		sDoc += "</table></body></html>";

		txtJSONRes.setText(sDoc);
		//JScrollBar vertical = scrollJSONRes.getVerticalScrollBar();
		//vertical.setValue(vertical.getMinimum());
	}


	// Рекурсивное формирование объекта в html
	private static void JSON_GenerateHTMLByNode(DocNode nodeDoc) {
		if (nodeDoc == null)
			return;

		// Количество повторений
		String sCount = "" + nodeDoc.minCount;
		if (nodeDoc.maxCount != nodeDoc.minCount) {
			if (nodeDoc.maxCount < 0)
				sCount += "-n";
			else
				sCount += "-" + nodeDoc.maxCount;
		}

		// Строка таблицы
		// Элемент
		sDoc += "<tr>" +
			HTMLtd((nodeDoc.level+corrlevel)-1) +
			"<td align='left' valign='top' colspan='" + ((maxDocLevel+corrlevel)-(nodeDoc.level+corrlevel)+1) + "'>" + Utils.htmlEscape(nodeDoc.id) + "</td>";

		// Тип
		sDoc += "<td align='left' valign='top'>";
		if (!nodeDoc.type.isEmpty())
			sDoc += Utils.htmlEscape(nodeDoc.type);
		else
			sDoc += Utils.htmlEscape(nodeDoc.ref);
		if (!nodeDoc.minLength.isEmpty())
			sDoc += "<br/>minLength: " + Utils.htmlEscape(nodeDoc.minLength);
		if (!nodeDoc.maxLength.isEmpty())
			sDoc += "<br/>maxLength: " + Utils.htmlEscape(nodeDoc.maxLength);
		if (!nodeDoc.minimum.isEmpty())
			sDoc += "<br/>minimum: " + Utils.htmlEscape(nodeDoc.minimum);
		if (!nodeDoc.maximum.isEmpty())
			sDoc += "<br/>maximum: " + Utils.htmlEscape(nodeDoc.maximum);
		//if (!nodeDoc.minItems.isEmpty())
		//	sDoc += "<br/>minItems: " + Utils.htmlEscape(nodeDoc.minItems);
		//if (!nodeDoc.maxItems.isEmpty())
		//	sDoc += "<br/>maxItems: " + Utils.htmlEscape(nodeDoc.maxItems);
		//if (!nodeDoc.minProperties.isEmpty())
		//	sDoc += "<br/>minProperties: " + Utils.htmlEscape(nodeDoc.minProperties);
		//if (!nodeDoc.maxProperties.isEmpty())
		//	sDoc += "<br/>maxProperties: " + Utils.htmlEscape(nodeDoc.maxProperties);
		if (!nodeDoc.uniqueItems.isEmpty())
			sDoc += "<br/>uniqueItems: " + Utils.htmlEscape(nodeDoc.uniqueItems);
		if (!nodeDoc.pattern.isEmpty())
			sDoc += "<br/>pattern: " + Utils.htmlEscape(nodeDoc.pattern);
		if (!nodeDoc.senum.isEmpty())
			sDoc += "<br/>enum: [" + Utils.htmlEscape(nodeDoc.senum) + "]";
		if (!nodeDoc.format.isEmpty())
			sDoc += "<br/>format: " + Utils.htmlEscape(nodeDoc.format);
		sDoc += "</td>";

		// Описание
		sDoc += "<td align='left' valign='top'>";
		if (!nodeDoc.description.isEmpty())
			sDoc += "<p>" + Utils.htmlEscape(nodeDoc.description) + "</p>";
		if (!nodeDoc.example.isEmpty())
			sDoc += "<p>Пример: " + Utils.htmlEscape("\"" + nodeDoc.example + "\"") + "</p>";
		if (!nodeDoc.example_default.isEmpty())
			sDoc += "<p>Пример: " + Utils.htmlEscape("\"" + nodeDoc.example_default + "\"") + "</p>";
		if (!nodeDoc.examples.isEmpty())
			sDoc += "<p>Примеры: " + Utils.htmlEscape(nodeDoc.examples) + "</p>";
		sDoc += "</td>";

		// Кратность
		sDoc += "<td align='left' valign='top'>" + sCount + "</td></tr>";

		// Пройти по дочерним объектам
		for (DocNode node : nodeDoc.childs) {
			JSON_GenerateHTMLByNode(node);
		}
	}

	// Сгенерить <td></td> в количестве n штук
	private static String HTMLtd(int n) {
		if (n <= 0)
			return "";

		String res = "";
		for (int i = 0; i < n; i++) {
			res += "<td></td>";
		}
		return res;
	}



	private static String SUP_PathToStr(File fileSUP_old, File fileSUP_new) {
		String sfile_old = "";
		if (fileSUP_old != null)
			sfile_old = fileSUP_old.toString();

		String sfile_new = "";
		if (fileSUP_new != null)
			sfile_new = fileSUP_new.toString();

		return "old = " + sfile_old + "<br>new = " + sfile_new;
	}


	// Чтение json-конфига
	private static void SUP_Read(File fileSUP, ArrayList<SUP> SUPcur) {
		String sfile = "";
		if (fileSUP != null)
			sfile = fileSUP.toString();

		if (sfile.isEmpty())
			return;

		try {
			// Прочитать json
			String sJson = Utils.readFileToStr(sfile);
			//txtJSONRes.setText(sJson);
			//txtJSONRes.setText(fileJSON.getName());

			// Получить корневой объект
			mapper = new ObjectMapper();
			JsonNode rootNode = mapper.readValue(sJson, JsonNode.class);
			JsonNode definitionsNode = null;
			if (rootNode != null) {
				// parameters
				JsonNode parametersNode = rootNode.get("parameters");
				if (parametersNode != null) {
					if (parametersNode.isArray()) {
						for (JsonNode node : parametersNode) {
							SUP param = new SUP();
							// Название
							param.name = Utils.jsonReadStr(node, "name" ,"").trim();

							// Описание
							param.description = Utils.jsonReadStr(node, "description" ,"").trim();

							// Тип
							String sType = Utils.jsonReadStr(node, "type" ,"").trim();
							String sList = Utils.jsonReadStr(node, "isList" ,"").trim();
							if (sList.equals("true"))
								sType = "list<" + sType + ">";
							param.type = sType;

							// Роль
							String sRoles = "";
							JsonNode rolesNode = node.get("roles");
							if (rolesNode != null) {
								if (rolesNode.isArray()) {
									for (JsonNode node2 : rolesNode) {
										String s = Utils.isNull(node2.asText(), "");
										if (sRoles.isEmpty())
											sRoles = s;
										else
											sRoles += ", " + s;
									}
								}
							}
							param.roles = sRoles;

							// bundle
							param.channel = "";
							param.values = "";
							JsonNode bundleNode = node.get("bundle");
							if (bundleNode != null) {
								if (bundleNode.isArray()) {
									for (JsonNode node3 : bundleNode) {

										// path
										JsonNode pathNode = node3.get("path");
										if (pathNode != null) {
											if (pathNode.isArray()) {
												for (JsonNode node4 : pathNode) {
													// Канал
													String sCode = Utils.jsonReadStr(node4, "code" ,"").trim();
													if (sCode.equals("CHANNEL")) {
														if (param.channel.isEmpty())
															param.channel = Utils.jsonReadStr(node4, "value" ,"").trim();
														else
															param.channel += "\n----\n" + Utils.jsonReadStr(node4, "value" ,"").trim();
													}
												}
											}
										}

										// values
										String values2 = "";
										JsonNode valuesNode = node3.get("values");
										if (valuesNode != null) {
											if (valuesNode.isArray()) {
												for (JsonNode node5 : valuesNode) {
													String s = Utils.isNull(node5.asText(), "");
													if (values2.isEmpty())
														values2 = s;
													else
														values2 += ", " + s;
												}
											}
										}
										if (param.values.isEmpty())
											param.values = values2;
										else
											param.values += "\n----\n" + values2;


										//break;
									}
								}
							}

							SUPcur.add(param);
						}
					}
				}
			}

			// Сортировать
			if (cbSUPSort.isSelected()) {
				//Collections.sort(SUPcur);
				/*Collections.sort(SUPcur, new Comparator<SUP>() {
					@Override
					public int compare(SUP lhs, SUP rhs) {
						// -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
						lhs.name.compareToIgnoreCase(rhs.name);
						//return lhs.name > rhs.name ? -1 : (lhs.name < rhs.name) ? 1 : 0;
					}
				});*/

				//Collections.sort(SUPcur, (a, b) -> b.compareTo(a));
				SUPcur.sort((a, b) -> a.name.compareToIgnoreCase(b.name)); // по возрастанию
				//Collections.sort(SUPcur, (a, b) -> b.name.compareToIgnoreCase(a.name) ); // по убыванию
			}

		}
		catch (Exception ex) {
			txtJSONRes.setText(ex.getMessage());
		}
	}


	// Генератор документации ФП СУП в формате HTML
	private static void SUP_GenerateHTML() {
		txtSUPRes.setText("");

		if (SUP_new.size() == 0) {
			txtSUPRes.setText("Выберите как минимум новый конфиг");
			return;
		}

		String sDoc1 = "<html><head><style type='text/css'>body {font-family: Sans; font-size: 14;}</style></head><body>";

		// Дубли
		boolean existsDouble = false;
		String sDocDouble = "";

		int i1 = 0;
		for (SUP param : SUP_new) {
			i1 += 1;

			boolean isFind = false; // найден дубль
			int i2 = 0;
			for (SUP param2 : SUP_new) {
				i2 += 1;
				if ( i2 != i1 && param.name.equalsIgnoreCase(param2.name)) {
					isFind = true;
					break;
				}
			}

			if (isFind) {
				sDocDouble += "<li>" + Utils.htmlEscape(param.name) + "</li>";
				existsDouble = true;
			}
		}

		// Добавлены параметры
		boolean existsNew = false;
		String sDocNew = "";

		// Удалены параметры
		boolean existsDel = false;
		String sDocDel = "";

		// Изменения параметров
		boolean existsChange = false;
		String sDocChange = "";


		// Заголовок таблицы
		String sDoc2 = "<p></p><table align='left' border='1' class='wrapped'>";
		sDoc2 += "<tr>" +
			"<th align='left' valign='top'>Название</th>" +
			"<th align='left' valign='top'>Тип</th>" +
			"<th align='left' valign='top'>Канал</th>" +
			"<th align='left' valign='top'>Значение по умолчанию</th>" +
			"<th align='left' valign='top'>Описание</th>" +
			"<th align='left' valign='top'>Роль</th>" +
			"</tr>";

		// Новые и измененные параметры
		for (SUP param : SUP_new) {
			sDoc2 += "<tr>";
			//bgcolor='#00ff00' зеленый
			//bgcolor='#ffff00' желтый

			String bgcolorNew = "";
			String bgcolorChange = "";
			boolean isDocChanged = false; // чтоб второй раз не добавлять параметр в список измененнных

			// Новый параметр?
			boolean isNew = false;
			SUP paramFinded = null;
			if (SUP_old.size() > 0) {
				boolean isFind = false;
				for (SUP param2 : SUP_old) {
					if (param.name.equalsIgnoreCase(param2.name)) {
						isFind = true;
						paramFinded = param2;
						break;
					}
				}
				if (!isFind) {
					isNew = true;
					bgcolorNew = " bgcolor='#00ff00' class='highlight-green' data-highlight-colour='green' "; // зеленый
					existsNew = true;
					sDocNew += "<li>" + Utils.htmlEscape(param.name) + "</li>";
				}
			}

			// Название
			sDoc2 +=  "<td align='left' valign='top' " + bgcolorNew + ">" + Utils.htmlEscape(param.name) + "</td>";

			// Тип
			bgcolorChange = "";
			if (paramFinded != null)
			{
				if (!param.type.equalsIgnoreCase(paramFinded.type)) {
					bgcolorChange = " bgcolor='#ffff00' class='highlight-yellow' data-highlight-colour='yellow' "; // желтый
					existsChange = true;
					if (!isDocChanged) {
						sDocChange += "<li>" + Utils.htmlEscape(param.name) + "</li>";
						isDocChanged = true;
					}
				}
			}
			if (!bgcolorNew.isEmpty())
				sDoc2 += "<td align='left' valign='top' " + bgcolorNew + ">" + Utils.htmlEscape(param.type) + "</td>";
			else if (!bgcolorChange.isEmpty())
				sDoc2 += "<td align='left' valign='top' " + bgcolorChange + ">" + Utils.htmlEscape(param.type) + "</td>";
			else
				sDoc2 += "<td align='left' valign='top'>" + Utils.htmlEscape(param.type) + "</td>";

			// Канал
			bgcolorChange = "";
			if (paramFinded != null)
			{
				if (!param.channel.equalsIgnoreCase(paramFinded.channel)) {
					bgcolorChange = " bgcolor='#ffff00' class='highlight-yellow' data-highlight-colour='yellow' "; // желтый
					existsChange = true;
					if (!isDocChanged) {
						sDocChange += "<li>" + Utils.htmlEscape(param.name) + "</li>";
						isDocChanged = true;
					}
				}
			}
			if (!bgcolorNew.isEmpty())
				sDoc2 += "<td align='left' valign='top' " + bgcolorNew + ">" + Utils.htmlEscape(param.channel) + "</td>";
			else if (!bgcolorChange.isEmpty())
				sDoc2 += "<td align='left' valign='top' " + bgcolorChange + ">" + Utils.htmlEscape(param.channel) + "</td>";
			else
				sDoc2 += "<td align='left' valign='top'>" + Utils.htmlEscape(param.channel) + "</td>";

			// Значение по умолчанию
			bgcolorChange = "";
			if (paramFinded != null)
			{
				if (!param.values.equalsIgnoreCase(paramFinded.values)) {
					bgcolorChange = " bgcolor='#ffff00' class='highlight-yellow' data-highlight-colour='yellow' "; // желтый
					existsChange = true;
					if (!isDocChanged) {
						sDocChange += "<li>" + Utils.htmlEscape(param.name) + "</li>";
						isDocChanged = true;
					}
				}
			}
			if (!bgcolorNew.isEmpty())
				sDoc2 += "<td align='left' valign='top' " + bgcolorNew + ">" + Utils.htmlEscape(param.values) + "</td>";
			else if (!bgcolorChange.isEmpty())
				sDoc2 += "<td align='left' valign='top' " + bgcolorChange + ">" + Utils.htmlEscape(param.values) + "</td>";
			else
				sDoc2 += "<td align='left' valign='top'>" + Utils.htmlEscape(param.values) + "</td>";

			// Описание
			bgcolorChange = "";
			if (paramFinded != null)
			{
				if (!param.description.equalsIgnoreCase(paramFinded.description)) {
					bgcolorChange = " bgcolor='#ffff00' class='highlight-yellow' data-highlight-colour='yellow' "; // желтый
					existsChange = true;
					if (!isDocChanged) {
						sDocChange += "<li>" + Utils.htmlEscape(param.name) + "</li>";
						isDocChanged = true;
					}
				}
			}
			if (!bgcolorNew.isEmpty())
				sDoc2 += "<td align='left' valign='top' " + bgcolorNew + ">" + Utils.htmlEscape(param.description) + "</td>";
			else if (!bgcolorChange.isEmpty())
				sDoc2 += "<td align='left' valign='top' " + bgcolorChange + ">" + Utils.htmlEscape(param.description) + "</td>";
			else
				sDoc2 += "<td align='left' valign='top'>" + Utils.htmlEscape(param.description) + "</td>";

			// Роль
			bgcolorChange = "";
			if (paramFinded != null)
			{
				if (!param.roles.equalsIgnoreCase(paramFinded.roles)) {
					bgcolorChange = " bgcolor='#ffff00' class='highlight-yellow' data-highlight-colour='yellow' "; // желтый
					existsChange = true;
					if (!isDocChanged) {
						sDocChange += "<li>" + Utils.htmlEscape(param.name) + "</li>";
						isDocChanged = true;
					}
				}
			}
			if (!bgcolorNew.isEmpty())
				sDoc2 += "<td align='left' valign='top' " + bgcolorNew + ">" + Utils.htmlEscape(param.roles) + "</td>";
			else if (!bgcolorChange.isEmpty())
				sDoc2 += "<td align='left' valign='top' " + bgcolorChange + ">" + Utils.htmlEscape(param.roles) + "</td>";
			else
				sDoc2 += "<td align='left' valign='top'>" + Utils.htmlEscape(param.roles) + "</td>";

			//
			sDoc2 += "</tr>";
		}


		// Удаленные параметры
		for (SUP param : SUP_old) {
			String bgcolorDel = "";

			// Удаленный параметр?
			boolean isDel = false;
			boolean isFind = false;
			for (SUP param2 : SUP_new) {
				if (param.name.equalsIgnoreCase(param2.name)) {
					isFind = true;
					break;
				}
			}
			if (!isFind) {
				isDel = true;
				bgcolorDel = " bgcolor='#ff0000' class='highlight-red' data-highlight-colour='red' "; // красный
				existsDel = true;
				sDocDel += "<li>" + Utils.htmlEscape(param.name) + "</li>";
			}

			if (isDel) {
				sDoc2 += "<tr>";
				// Название
				sDoc2 += "<td align='left' valign='top' " + bgcolorDel + ">" + Utils.htmlEscape(param.name) + "</td>";
				// Тип
				sDoc2 += "<td align='left' valign='top' " + bgcolorDel + ">" + Utils.htmlEscape(param.type) + "</td>";
				// Канал
				sDoc2 += "<td align='left' valign='top' " + bgcolorDel + ">" + Utils.htmlEscape(param.channel) + "</td>";
				// Значение по умолчанию
				sDoc2 += "<td align='left' valign='top' " + bgcolorDel + ">" + Utils.htmlEscape(param.values) + "</td>";
				// Описание
				sDoc2 += "<td align='left' valign='top' " + bgcolorDel + ">" + Utils.htmlEscape(param.description) + "</td>";
				// Роль
				sDoc2 += "<td align='left' valign='top' " + bgcolorDel + ">" + Utils.htmlEscape(param.roles) + "</td>";
				//
				sDoc2 += "</tr>";
			}
		}

		sDoc2 += "</table>";



		sDoc = sDoc1;

		if (existsDouble)
			sDoc += "<b><p>Дубли:<br><ul>" + sDocDouble + "</ul></p></b>";
		if (existsNew)
			sDoc += "<p>Добавлены параметры:<br><ul>" + sDocNew + "</ul></p>";
		if (existsDel)
			sDoc += "<p>Удалены параметры:<br><ul>" + sDocDel + "</ul></p>";
		if (existsChange)
			sDoc += "<p>Изменения параметров:<br><ul>" + sDocChange + "</ul></p>";

		sDoc += sDoc2;
		sDoc += "</body></html>";
		txtSUPRes.setText(sDoc);
	}


}


