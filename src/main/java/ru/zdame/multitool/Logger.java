package ru.zdame.multitool;

import java.time.LocalDateTime;

public class Logger extends Thread {

	// Добавить сообщение в лог
	public static void add(String s) {
		System.out.println(Utils.date2str(LocalDateTime.now()) + ": " + s);
	}

}